import { Injectable } from '@angular/core';
import { FilmModel } from './film.model';
import { map, Subject } from 'rxjs';
import { HttpClient } from '@angular/common/http';


@Injectable()
export class FilmService {
  filmsChange = new Subject<FilmModel[]>();
  filmsFetching = new Subject<boolean>();

  constructor(private http: HttpClient) {
  }

  private films: FilmModel[] = [];

  fetchFilms() {
    this.filmsFetching.next(true);
    this.http.get('https://newcinema-12a93-default-rtdb.firebaseio.com/films.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          // @ts-ignore
          const filmData = result[id];
          return new FilmModel(
            id,
            filmData.name,
          );
        });
      }))
      .subscribe(films => {
        this.films = films;
        this.filmsChange.next(this.films.slice());
        this.filmsFetching.next(false);
      }, error => {
        this.filmsFetching.next(false);
      });
  }

  getFilms() {
    return this.films.slice();
  }

  addFilms(film: object) {
    this.filmsFetching.next(true);
    this.http.post('https://newcinema-12a93-default-rtdb.firebaseio.com/films.json', film)
      .subscribe(() => {
        this.fetchFilms();
        this.filmsFetching.next(false);
      });
  }

  deleteFilm(film: FilmModel) {
    this.filmsFetching.next(true);
    this.http.delete(`https://newcinema-12a93-default-rtdb.firebaseio.com/films/${film.id}.json`)
      .subscribe( () => {
        this.fetchFilms();
        this.filmsFetching.next(false);
      });
  }
}

