import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CinemaComponent } from './cinema/cinema.component';
import { FilmComponent } from './cinema/film/film.component';

const routes: Routes = [
  {
    path: '', component: CinemaComponent, children: [
      {path: '', component: FilmComponent}
    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}
