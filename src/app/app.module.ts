import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CinemaComponent } from './cinema/cinema.component';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';
import { FilmService } from './shared/film.service';
import { FilmComponent } from './cinema/film/film.component';
import { NewFilmComponent } from './cinema/new-film/new-film.component';

@NgModule({
  declarations: [
    AppComponent,
    CinemaComponent,
    FilmComponent,
    NewFilmComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule
  ],
  providers: [FilmService],
  bootstrap: [AppComponent]
})
export class AppModule { }
