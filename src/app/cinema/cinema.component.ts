import { Component, OnDestroy, OnInit } from '@angular/core';
import { FilmModel } from '../shared/film.model';
import { Subscription } from 'rxjs';
import { FilmService } from '../shared/film.service';

@Component({
  selector: 'app-cinema',
  templateUrl: './cinema.component.html',
  styleUrls: ['./cinema.component.css']
})
export class CinemaComponent implements OnInit, OnDestroy {
  films: FilmModel[] = [];
  filmChangeSubscription!: Subscription;
  filmFetchingSubscription!: Subscription;
  isFetching: boolean = false;

  constructor(private filmService: FilmService) { }

  ngOnInit(): void {
    this.films = this.filmService.getFilms();
    this.filmChangeSubscription = this.filmService.filmsChange.subscribe((films: FilmModel[]) => {
      this.films = films;
    });
    this.filmFetchingSubscription = this.filmService.filmsFetching.subscribe((isFetching: boolean) => {
      this.isFetching = isFetching;
    });
    this.filmService.fetchFilms();
  }

  ngOnDestroy() {
    this.filmChangeSubscription.unsubscribe();
    this.filmFetchingSubscription.unsubscribe();
  }


}
