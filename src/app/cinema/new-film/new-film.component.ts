import { Component, ElementRef, ViewChild } from '@angular/core';
import { FilmService } from '../../shared/film.service';

@Component({
  selector: 'app-new-film',
  templateUrl: './new-film.component.html',
  styleUrls: ['./new-film.component.css']
})
export class NewFilmComponent {
  @ViewChild('movieNameInput') movieNameInput!: ElementRef;

  constructor(public filmService: FilmService) {
  }

  addFilm() {
    const name: string = this.movieNameInput.nativeElement.value;
    const film = {name};
    this.filmService.addFilms(film);

  }

}
