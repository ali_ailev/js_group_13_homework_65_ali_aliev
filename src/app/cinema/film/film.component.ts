import { Component, Input } from '@angular/core';
import { FilmModel } from '../../shared/film.model';
import { FilmService } from '../../shared/film.service';

@Component({
  selector: 'app-film',
  templateUrl: './film.component.html',
  styleUrls: ['./film.component.css']
})
export class FilmComponent {
  @Input() film!: FilmModel;

  constructor(public filmService: FilmService) {
  }

  onClick() {
    this.filmService.deleteFilm(this.film);
  }
}
